﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Substance.Game;
using UnityEngine.UI;

public class SubstanceInput : MonoBehaviour {
   public SubstanceGraph[] sub;
    public Transform content;
    public GameObject UISub;
    public MeshRenderer house;
    public MeshRenderer cube;
    public MeshRenderer sphere;
    public MarbleInput mar;
    private int subNum;

    // Use this for initialization
    void Start () {
       populateList();
        //Changes material and rebuilds UI
        mar.AssignGraph(sub[0]);
	}
	
    public void populateList()
    { int i = 0;
        foreach (SubstanceGraph s in sub)
        {
            Debug.Log(s.graphLabel);
            GameObject newSubstance = Instantiate(UISub, content, false);
            SubstanceName subUI = newSubstance.GetComponent<SubstanceName>();
            subUI.subName.text = s.name;
            subUI.subButton.onClick.AddListener(() =>
            {
                SubstanceGraph graph = getGraphFromName(subUI.subName.text);
                house.material = graph.material;
                cube.material = graph.material;
                sphere.material = graph.material;
                mar.AssignGraph(graph);
            });
        }
    }

    public SubstanceGraph getGraphFromName(string name)
    { 
        foreach(SubstanceGraph s in sub)
        {
            if(s.name.Equals(name))
            {
                return s;
            }
        }
        return null;
    }
	// Update is called once per frame
	void Update () {
		
	}
}
