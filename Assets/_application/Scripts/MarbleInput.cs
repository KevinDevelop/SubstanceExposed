﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Substance.Game;
using UnityEngine.UI;
using uCP;
using System.IO;
using System;

public class MarbleInput : MonoBehaviour {
    private SubstanceGraph mySubstance;
    public GameObject UISlider;
    public GameObject UIGroup;
    public GameObject UIRandom;
    public GameObject UIColor;
    public GameObject colorPickerWindow;
    public Transform content;
    public MeshRenderer houseMesh;
    public Button exportButton;
    
   // public ColorPicker colorPicker;
  
  

    private void Start()
    {
       
    }
    public void AssignGraph(SubstanceGraph g)
    { 
        float f = .1f;
        mySubstance = g;
        mySubstance.material.mainTextureScale = new Vector2(f,f);

        buildUI();
    }

    public void writeToFile(string s)
    {   //WINDOWS DOC PATH
        string mydocpath =
          Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        Debug.Log(s);
        using (StreamWriter sw = new StreamWriter(Path.Combine(mydocpath, mySubstance.name + ".txt")))
        {
            sw.Write(s);
        }

    }

    public void export()
    {
        exportButton.onClick.AddListener(() =>
        {
            Debug.Log("Export button hit");
            SubstanceGraph.InputProperties[] properties = mySubstance.GetInputProperties();
            String s = "{\n";
            foreach(SubstanceGraph.InputProperties input in properties)
            {
                if (input.label.Equals("$randomseed"))
                {
                    s += "\"" + input.name + "\": " + "" + mySubstance.GetInputFloat(input.name) + "," + "\n";

                }
                if (input.type == SubstanceGraph.InputPropertiesType.Float && !input.label.Equals("$randomseed"))
                {
                    s += "\"" + input.name + "\": " + "" + mySubstance.GetInputFloat(input.name) + "," + "\n";
                }
                if (input.type == SubstanceGraph.InputPropertiesType.Color)
                {
                    Color32 c = mySubstance.GetInputColor(input.name, 2);
                    int r = c.r;
                    int g = c.g;
                    int b = c.b;
                    int a = c.a;

                    uint bgrValue = ((uint)r << 24) + ((uint)g << 16) + ((uint)b << 8) + (uint)a;

                    s += "\"" + input.name + "\": " + "" + bgrValue + "," + "\n";
                }
                
            }
            s = s.Substring(0, s.Length - 2);
            s += "\n}";
            writeToFile(s);
        });
    }
    public void buildUI()
    {
        export();
        int size = mySubstance.GetInputProperties().Length;
        SubstanceGraph.InputProperties[] properties = mySubstance.GetInputProperties();
        int i = 0;
        string lastGroupName = "";
        //remove children
        foreach(Transform child in content)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (SubstanceGraph.InputProperties input in properties)
        {
            i++;
            // Debug.Log("input " + i + " " + input.label);
            //Debug.Log("type " + i + " " + input.type.ToString());
            //Debug.Log("Max " + input.maximum + " Min " + input.minimum);
            // Debug.Log("Group " + input.group);
            //texture scale slider
          
            string group = input.group;
            if (!lastGroupName.Equals(group) && i > 1)
            {
                if (group.Equals("Channels"))
                {
                    continue;
                }
                GameObject groupName = Instantiate(UIGroup, content, false);
                GroupInput g = groupName.GetComponent<GroupInput>();
                g.groupName.text = group;
                lastGroupName = group;
            }
            if (input.label.Equals("$outputsize"))
            {
                continue;
            }
            else if (input.label.Equals("Normal Type"))
            {
                continue;
            }
            else if (input.label.Equals("$randomseed"))
            {   
                GameObject rand = Instantiate(UIRandom, content, false);
                RandomInput randomInputScript = rand.GetComponent<RandomInput>();
                randomInputScript.randomButton.onClick.AddListener(() =>
                {
                    float r = UnityEngine.Random.Range(0.0f, 100.0f);
                    mySubstance.SetInputFloat(input.name, r);
                    mySubstance.QueueForRender();
                });
                //create texture scale slider
                float min = 0.01f;
                float max = 5.0f;
                GameObject slider = Instantiate(UISlider, content, false);
                SliderInput slide = slider.GetComponent<SliderInput>();
                Vector2 originTextureScale = mySubstance.material.mainTextureScale;
                Debug.Log("texture scale " + originTextureScale);
                slide.slider.value = originTextureScale.x;
                slide.sliderName.text = "Scale";
                slide.slider.minValue = min;
                slide.slider.maxValue = max;
                slide.slider.onValueChanged.AddListener(newValue =>
                {
                    mySubstance.material.mainTextureScale = new Vector2(newValue, newValue);

                });
                continue;
            }
         
            if (input.type == SubstanceGraph.InputPropertiesType.Color)
            {
                GameObject colorBar = Instantiate(UIColor, content, false);
               ColorPickerImage cp = colorBar.GetComponent<ColorPickerImage>();
                Color c = mySubstance.GetInputColor(input.name, 2);
                Debug.Log(c);
                cp.imageColor.color = c;
                 
                 cp.changeColor.onClick.AddListener(() =>
                 {
                     colorPickerWindow.SetActive(true);
                   ColorPickerDialogue cpd = colorPickerWindow.GetComponent<ColorPickerDialogue>();
                     
                     cpd.selectColor(c, (col) =>
                     {
                         //call when color changes
                         cp.imageColor.color = col;
                         mySubstance.SetInputColor(input.name, col);
                         mySubstance.QueueForRender();
                     });
                 });
               ColorPickerDialogue cpc = colorPickerWindow.GetComponent<ColorPickerDialogue>();
                cpc.closeButton.onClick.AddListener(() =>
                {
                    colorPickerWindow.SetActive(false);
                });
            }
            if (input.type == SubstanceGraph.InputPropertiesType.Float)
            {
                Vector4 vecMin = input.minimum;
                Vector4 vecMax = input.maximum;
               float min = vecMin.x;
                float max = vecMax.x;
               GameObject slider = Instantiate(UISlider, content, false);
               SliderInput slide = slider.GetComponent<SliderInput>();
                slide.slider.value = mySubstance.GetInputFloat(input.name);
                slide.sliderName.text = input.name;
                slide.slider.minValue = min;
                slide.slider.maxValue = max;
                slide.slider.onValueChanged.AddListener(newValue => {
                    mySubstance.SetInputFloat(input.name, newValue);
                    mySubstance.QueueForRender();
                });
            }
        }
    }

   
}
