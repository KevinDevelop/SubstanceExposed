﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Substance.Game;

public class DropdownList : MonoBehaviour {

    public Dropdown dropdown;
    public SubstanceGraph[] sub;
    public MeshRenderer mesh;
    public MarbleInput mar;

    void Start () {
        populateList();
        mar.AssignGraph(sub[0]);
        dropdown.onValueChanged.AddListener(delegate {
            int num = dropdown.value;
            string s = dropdown.itemText.text;
            SubstanceGraph graph = sub[num];
            mar.AssignGraph(graph);
            mesh.material = graph.material;
        });
	}

    public void populateList()
    {
        List<string> list = new List<string>();
        int i = 0;
        foreach (SubstanceGraph s in sub)
        {
            i++;
            list.Add(s.graphLabel);
        }
        Debug.Log(i);
        dropdown.AddOptions(list);
    }
	// Update is called once per frame
	void Update () {
		
	}

}
