﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using uCP;

public class ColorPickerDialogue: MonoBehaviour {
    public Button closeButton;
    //public GameObject UIColor;
    public ColorPicker colorPicker;
    private Color currentColor;
    private System.Action<Color> onSetColor;

    public void selectColor(Color color, System.Action<Color> callBackFunc)
    {
        currentColor = color;
        onSetColor = callBackFunc;
        colorPicker.Show(color);
    }
       
    public void colorChanged(Color newColor)
    {
        if(onSetColor != null)
        {
            onSetColor(newColor);
            
        }

    }
}
